$(document).ready(function () {
    function onSlide(dom, callback) {
        var posStartX = 0,
            posStartY = 0,
            posEndX = 0,
            posEndY = 0;
        dom.on('touchstart', function (e) {
            posStartX = e.originalEvent.touches[0].pageX;
            posStartY = e.originalEvent.touches[0].pageY;
            $('.notice').fadeIn().fadeOut('slow');
        });
        dom.on('touchmove', function (e) {
            posEndX = e.originalEvent.touches[0].pageX;
            posEndY = e.originalEvent.touches[0].pageY;
        });
        dom.on('touchend', function (e) {
            callback({
                startX: posStartX,
                startY: posStartX,
                endX: posEndX,
                endY: posEndY,
                event: e
            });
        });
    }

    $('.main').fullpage({
        slidesNavigation: false,
        controlArrows: false,
        verticalCentered: false,
        afterRender: function () {
            $.fn.fullpage.setAllowScrolling(false, 'left');
        },
        afterSlideLoad: function (anchorLink, index, slideAnchor, slideIndex) {
            var loadedSlide = $(this),
                start = 0,
                map = 1,
                phead = 2,
                ptail = 12,
                end = 13,
                t = 0;
            if (slideIndex === start) {
                $.fn.fullpage.setAllowScrolling(false, 'left');
            } else if (slideIndex === end) {
                $.fn.fullpage.setAllowScrolling(false, 'right');
            } else {
                $.fn.fullpage.setAllowScrolling(true, 'left');
                $.fn.fullpage.setAllowScrolling(true, 'right');
            }
            if (slideIndex >= phead && slideIndex <= ptail) {
                loadedSlide.find('article').animate({
                    height: '25%'
                }, 3000);
                onSlide(loadedSlide.find('article'), function (data) {
                    if (Math.abs(data.endX - data.startX) > 10 && Math.abs(data.endY - data.startY) < 20) {
                        data.event.preventDefault();
                    }
                });
            } else if (slideIndex === end) {
                loadedSlide.find('p').each(function (idx, ele) {
                    $(ele).delay(idx * 2000).animate({
                        height: '11em'
                    }, 2000);
                });
            } else if (slideIndex === map) {
                $('iframe').contents().find("svg")[0].setAttribute('class', 'show');
                //$('iframe').contents().find("svg").addClass('show');//not work
            }
        }
    });
    $('html').fadeIn();
    $('audio')[0].play();
    $('article').perfectScrollbar({
        suppressScrollX: true
    });
    onSlide($('body'), function (data) {
        if (Math.abs(data.endY - data.startY) > 10 && Math.abs(data.endX - data.startX) < 20) {
            $('.notice').fadeIn().fadeOut('slow');
        }
    });
});
